﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empleados
{
    class Empleado
    {
        private int id;
        private String nombre;
        private String apll1;
        private String apll2;
        private DateTime fechaContrato;
        private Decimal sueldo;

        private int idTipoUsuario;
        private int idTipoContrato;
        private int idSexo;
        private int bHabilitado;
        private int tieneUsuario;
        private char tipoUsuario;

        //Constructor vacio 
        public Empleado() { }

        
        // CONSTRUCTOR CON TODOS LOS PARÁMETROS EXCEPTO EL 'id'
        public Empleado(string nombre, string apll1, string apll2, DateTime fechaContrato, Decimal sueldo,
            int idTipoUsuario, int idTipoContrato, int idSexo, int bHabilitado, int tieneUsuario, char tipoUsuario)
        {
            this.nombre = nombre;
            this.apll1 = apll1;
            this.apll2 = apll2;
            this.fechaContrato = fechaContrato;
            this.sueldo = sueldo;
            this.idTipoUsuario = idTipoUsuario;
            this.idTipoContrato = idTipoContrato;
            this.idSexo = idSexo;
            this.bHabilitado = bHabilitado;
            this.tieneUsuario = tieneUsuario;
            this.tipoUsuario = tipoUsuario;
        }

        //Cosntructor con TODOS los parámetros parametros
        public Empleado(int id, string nombre, string apll1, string apll2, DateTime fechaContrato, Decimal sueldo,
            int idTipoUsuario, int idTipoContrato, int idSexo, int bHabilitado, int tieneUsuario, char tipoUsuario)
        {
            this.id = id;
            this.nombre = nombre;
            this.apll1 = apll1;
            this.apll2 = apll2;
            this.fechaContrato = fechaContrato;
            this.sueldo = sueldo;
            this.idTipoUsuario = idTipoUsuario;
            this.idTipoContrato = idTipoContrato;
            this.idSexo = idSexo;
            this.bHabilitado = bHabilitado;
            this.tieneUsuario = tieneUsuario;
            this.tipoUsuario = tipoUsuario;
        }


        //SETTER Y GETTER
        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apll1 { get => apll1; set => apll1 = value; }
        public string Apll2 { get => apll2; set => apll2 = value; }
        public DateTime FechaContrato { get => fechaContrato; set => fechaContrato = value; }
        public Decimal Sueldo { get => sueldo; set => sueldo = value; }
        public int IdTipoUsuario { get => idTipoUsuario; set => idTipoUsuario = value; }
        public int IdTipoContrato { get => idTipoContrato; set => idTipoContrato = value; }
        public int IdSexo { get => idSexo; set => idSexo = value; }
        public int BHabilitado { get => bHabilitado; set => bHabilitado = value; }
        public int TieneUsuario { get => tieneUsuario; set => tieneUsuario = value; }
        public char TipoUsuario { get => tipoUsuario; set => tipoUsuario = value; }
    }
}
