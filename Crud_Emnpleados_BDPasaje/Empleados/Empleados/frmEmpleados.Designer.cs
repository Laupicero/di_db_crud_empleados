﻿
namespace Empleados
{
    partial class frmEmpleados
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmpleados));
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dgvEmpleado = new System.Windows.Forms.DataGridView();
            this.panelDatos = new System.Windows.Forms.Panel();
            this.panelNavigator = new System.Windows.Forms.Panel();
            this.bnEmpleado = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnAddNewEmployee = new System.Windows.Forms.ToolStripButton();
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnDeleteEmployeeItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnActualPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnEditEmployee = new System.Windows.Forms.ToolStripButton();
            this.bnSaveChanges = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnRefresh = new System.Windows.Forms.ToolStripButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFechaContrato = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.cmbTipousuario = new System.Windows.Forms.ComboBox();
            this.txtMaterno = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPaterno = new System.Windows.Forms.TextBox();
            this.cmbContrato = new System.Windows.Forms.ComboBox();
            this.txtSueldo = new System.Windows.Forms.TextBox();
            this.chkHabilitado = new System.Windows.Forms.CheckBox();
            this.cmbSexo = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpleado)).BeginInit();
            this.panelDatos.SuspendLayout();
            this.panelNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnEmpleado)).BeginInit();
            this.bnEmpleado.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(0, 127);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(894, 37);
            this.label1.TabIndex = 31;
            this.label1.Text = "EMPLEADOS";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(894, 127);
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            // 
            // dgvEmpleado
            // 
            this.dgvEmpleado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmpleado.Location = new System.Drawing.Point(20, 476);
            this.dgvEmpleado.Name = "dgvEmpleado";
            this.dgvEmpleado.Size = new System.Drawing.Size(839, 284);
            this.dgvEmpleado.TabIndex = 60;
            this.dgvEmpleado.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmpleado_CellValueChanged);
            this.dgvEmpleado.SelectionChanged += new System.EventHandler(this.dgvEmpleado_SelectionChanged);
            // 
            // panelDatos
            // 
            this.panelDatos.Controls.Add(this.panelNavigator);
            this.panelDatos.Controls.Add(this.label2);
            this.panelDatos.Controls.Add(this.label10);
            this.panelDatos.Controls.Add(this.label3);
            this.panelDatos.Controls.Add(this.dtpFechaContrato);
            this.panelDatos.Controls.Add(this.label4);
            this.panelDatos.Controls.Add(this.label5);
            this.panelDatos.Controls.Add(this.label6);
            this.panelDatos.Controls.Add(this.label7);
            this.panelDatos.Controls.Add(this.lblCodigo);
            this.panelDatos.Controls.Add(this.label9);
            this.panelDatos.Controls.Add(this.txtNombre);
            this.panelDatos.Controls.Add(this.cmbTipousuario);
            this.panelDatos.Controls.Add(this.txtMaterno);
            this.panelDatos.Controls.Add(this.label8);
            this.panelDatos.Controls.Add(this.txtPaterno);
            this.panelDatos.Controls.Add(this.cmbContrato);
            this.panelDatos.Controls.Add(this.txtSueldo);
            this.panelDatos.Controls.Add(this.chkHabilitado);
            this.panelDatos.Controls.Add(this.cmbSexo);
            this.panelDatos.Location = new System.Drawing.Point(20, 167);
            this.panelDatos.Name = "panelDatos";
            this.panelDatos.Size = new System.Drawing.Size(839, 292);
            this.panelDatos.TabIndex = 61;
            // 
            // panelNavigator
            // 
            this.panelNavigator.Controls.Add(this.bnEmpleado);
            this.panelNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelNavigator.Location = new System.Drawing.Point(0, 0);
            this.panelNavigator.Name = "panelNavigator";
            this.panelNavigator.Size = new System.Drawing.Size(839, 27);
            this.panelNavigator.TabIndex = 62;
            // 
            // bnEmpleado
            // 
            this.bnEmpleado.AddNewItem = this.bnAddNewEmployee;
            this.bnEmpleado.CountItem = this.bnCountItem;
            this.bnEmpleado.DeleteItem = this.bnDeleteEmployeeItem;
            this.bnEmpleado.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bnActualPositionItem,
            this.bnCountItem,
            this.bindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bnAddNewEmployee,
            this.bnEditEmployee,
            this.bnSaveChanges,
            this.bnDeleteEmployeeItem,
            this.toolStripSeparator,
            this.toolStripSeparator1,
            this.bnRefresh});
            this.bnEmpleado.Location = new System.Drawing.Point(0, 0);
            this.bnEmpleado.MoveFirstItem = this.bnMoveFirstItem;
            this.bnEmpleado.MoveLastItem = this.bnMoveLastItem;
            this.bnEmpleado.MoveNextItem = this.bnMoveNextItem;
            this.bnEmpleado.MovePreviousItem = this.bnMovePreviousItem;
            this.bnEmpleado.Name = "bnEmpleado";
            this.bnEmpleado.PositionItem = this.bnActualPositionItem;
            this.bnEmpleado.Size = new System.Drawing.Size(839, 25);
            this.bnEmpleado.TabIndex = 3;
            this.bnEmpleado.Text = "bindingNavigator1";
            // 
            // bnAddNewEmployee
            // 
            this.bnAddNewEmployee.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnAddNewEmployee.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewEmployee.Image")));
            this.bnAddNewEmployee.Name = "bnAddNewEmployee";
            this.bnAddNewEmployee.RightToLeftAutoMirrorImage = true;
            this.bnAddNewEmployee.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewEmployee.Text = "Agregar Empleado";
            this.bnAddNewEmployee.Click += new System.EventHandler(this.bnAddNewEmployee_Click);
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(37, 22);
            this.bnCountItem.Text = "de {0}";
            this.bnCountItem.ToolTipText = "Número total de elementos";
            // 
            // bnDeleteEmployeeItem
            // 
            this.bnDeleteEmployeeItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnDeleteEmployeeItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteEmployeeItem.Image")));
            this.bnDeleteEmployeeItem.Name = "bnDeleteEmployeeItem";
            this.bnDeleteEmployeeItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteEmployeeItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteEmployeeItem.Text = "Eliminar Empleado";
            this.bnDeleteEmployeeItem.Click += new System.EventHandler(this.bnDeleteEmployeeItem_Click_1);
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Mover primero";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.bnMoveFirstItem_Click);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Mover anterior";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.bnMovePreviousItem_Click);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnActualPositionItem
            // 
            this.bnActualPositionItem.AccessibleName = "Posición";
            this.bnActualPositionItem.AutoSize = false;
            this.bnActualPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bnActualPositionItem.Name = "bnActualPositionItem";
            this.bnActualPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnActualPositionItem.Text = "0";
            this.bnActualPositionItem.ToolTipText = "Posición actual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Mover siguiente";
            this.bnMoveNextItem.Click += new System.EventHandler(this.bnMoveNextItem_Click);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Mover último";
            this.bnMoveLastItem.Click += new System.EventHandler(this.bnMoveLastItem_Click);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnEditEmployee
            // 
            this.bnEditEmployee.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEditEmployee.Image = ((System.Drawing.Image)(resources.GetObject("bnEditEmployee.Image")));
            this.bnEditEmployee.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEditEmployee.Name = "bnEditEmployee";
            this.bnEditEmployee.Size = new System.Drawing.Size(23, 22);
            this.bnEditEmployee.Text = "Modificar Empleado";
            this.bnEditEmployee.Click += new System.EventHandler(this.bnEditEmployee_Click);
            // 
            // bnSaveChanges
            // 
            this.bnSaveChanges.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnSaveChanges.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveChanges.Image")));
            this.bnSaveChanges.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnSaveChanges.Name = "bnSaveChanges";
            this.bnSaveChanges.Size = new System.Drawing.Size(23, 22);
            this.bnSaveChanges.Text = "Guardar Cambios";
            this.bnSaveChanges.Click += new System.EventHandler(this.bnSaveChanges_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnRefresh
            // 
            this.bnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("bnRefresh.Image")));
            this.bnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnRefresh.Name = "bnRefresh";
            this.bnRefresh.Size = new System.Drawing.Size(23, 22);
            this.bnRefresh.Text = "Actualizar";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Location = new System.Drawing.Point(111, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 15);
            this.label2.TabIndex = 64;
            this.label2.Text = "ID";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(106, 205);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 14);
            this.label10.TabIndex = 76;
            this.label10.Text = "FECHA CONTRATO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(111, 86);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 14);
            this.label3.TabIndex = 63;
            this.label3.Text = "NOMBRE";
            // 
            // dtpFechaContrato
            // 
            this.dtpFechaContrato.CustomFormat = "yyyy/MM/dd";
            this.dtpFechaContrato.Enabled = false;
            this.dtpFechaContrato.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechaContrato.Location = new System.Drawing.Point(236, 199);
            this.dtpFechaContrato.MaxDate = new System.DateTime(2108, 12, 31, 0, 0, 0, 0);
            this.dtpFechaContrato.MinDate = new System.DateTime(1953, 1, 1, 0, 0, 0, 0);
            this.dtpFechaContrato.Name = "dtpFechaContrato";
            this.dtpFechaContrato.Size = new System.Drawing.Size(142, 20);
            this.dtpFechaContrato.TabIndex = 75;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(111, 124);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.TabIndex = 62;
            this.label4.Text = "PATERNO";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(417, 122);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 14);
            this.label5.TabIndex = 61;
            this.label5.Text = "MATERNO";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(111, 162);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 14);
            this.label6.TabIndex = 60;
            this.label6.Text = "SUELDO";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(417, 169);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 14);
            this.label7.TabIndex = 59;
            this.label7.Text = "SEXO";
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Enabled = false;
            this.lblCodigo.Location = new System.Drawing.Point(164, 51);
            this.lblCodigo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(0, 13);
            this.lblCodigo.TabIndex = 58;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(417, 205);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 14);
            this.label9.TabIndex = 74;
            this.label9.Text = "TIPO USUARIO";
            // 
            // txtNombre
            // 
            this.txtNombre.Enabled = false;
            this.txtNombre.Location = new System.Drawing.Point(191, 81);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(562, 20);
            this.txtNombre.TabIndex = 68;
            // 
            // cmbTipousuario
            // 
            this.cmbTipousuario.Enabled = false;
            this.cmbTipousuario.FormattingEnabled = true;
            this.cmbTipousuario.Location = new System.Drawing.Point(551, 202);
            this.cmbTipousuario.Name = "cmbTipousuario";
            this.cmbTipousuario.Size = new System.Drawing.Size(198, 21);
            this.cmbTipousuario.TabIndex = 73;
            // 
            // txtMaterno
            // 
            this.txtMaterno.Enabled = false;
            this.txtMaterno.Location = new System.Drawing.Point(514, 117);
            this.txtMaterno.Name = "txtMaterno";
            this.txtMaterno.Size = new System.Drawing.Size(239, 20);
            this.txtMaterno.TabIndex = 67;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(417, 243);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 14);
            this.label8.TabIndex = 72;
            this.label8.Text = "TIPO DE CONTRATO";
            // 
            // txtPaterno
            // 
            this.txtPaterno.Enabled = false;
            this.txtPaterno.Location = new System.Drawing.Point(213, 119);
            this.txtPaterno.Name = "txtPaterno";
            this.txtPaterno.Size = new System.Drawing.Size(165, 20);
            this.txtPaterno.TabIndex = 66;
            // 
            // cmbContrato
            // 
            this.cmbContrato.Enabled = false;
            this.cmbContrato.FormattingEnabled = true;
            this.cmbContrato.Location = new System.Drawing.Point(551, 236);
            this.cmbContrato.Name = "cmbContrato";
            this.cmbContrato.Size = new System.Drawing.Size(198, 21);
            this.cmbContrato.TabIndex = 71;
            // 
            // txtSueldo
            // 
            this.txtSueldo.Enabled = false;
            this.txtSueldo.Location = new System.Drawing.Point(213, 162);
            this.txtSueldo.Name = "txtSueldo";
            this.txtSueldo.Size = new System.Drawing.Size(165, 20);
            this.txtSueldo.TabIndex = 65;
            // 
            // chkHabilitado
            // 
            this.chkHabilitado.AutoSize = true;
            this.chkHabilitado.Enabled = false;
            this.chkHabilitado.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHabilitado.Location = new System.Drawing.Point(109, 239);
            this.chkHabilitado.Name = "chkHabilitado";
            this.chkHabilitado.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkHabilitado.Size = new System.Drawing.Size(96, 18);
            this.chkHabilitado.TabIndex = 70;
            this.chkHabilitado.Text = "HABILITADO";
            this.chkHabilitado.UseVisualStyleBackColor = true;
            // 
            // cmbSexo
            // 
            this.cmbSexo.Enabled = false;
            this.cmbSexo.FormattingEnabled = true;
            this.cmbSexo.Items.AddRange(new object[] {
            "Masculino",
            "Femenido"});
            this.cmbSexo.Location = new System.Drawing.Point(551, 159);
            this.cmbSexo.Name = "cmbSexo";
            this.cmbSexo.Size = new System.Drawing.Size(198, 21);
            this.cmbSexo.TabIndex = 69;
            // 
            // frmEmpleados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 793);
            this.Controls.Add(this.panelDatos);
            this.Controls.Add(this.dgvEmpleado);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmEmpleados";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "E M P L E A D O S";
            this.Load += new System.EventHandler(this.frmEmpleados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpleado)).EndInit();
            this.panelDatos.ResumeLayout(false);
            this.panelDatos.PerformLayout();
            this.panelNavigator.ResumeLayout(false);
            this.panelNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnEmpleado)).EndInit();
            this.bnEmpleado.ResumeLayout(false);
            this.bnEmpleado.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dgvEmpleado;
        private System.Windows.Forms.Panel panelDatos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpFechaContrato;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.ComboBox cmbTipousuario;
        private System.Windows.Forms.TextBox txtMaterno;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPaterno;
        private System.Windows.Forms.ComboBox cmbContrato;
        private System.Windows.Forms.TextBox txtSueldo;
        private System.Windows.Forms.CheckBox chkHabilitado;
        private System.Windows.Forms.ComboBox cmbSexo;
        private System.Windows.Forms.Panel panelNavigator;
        private System.Windows.Forms.BindingNavigator bnEmpleado;
        private System.Windows.Forms.ToolStripButton bnAddNewEmployee;
        private System.Windows.Forms.ToolStripLabel bnCountItem;
        private System.Windows.Forms.ToolStripButton bnDeleteEmployeeItem;
        private System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bnActualPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bnMoveNextItem;
        private System.Windows.Forms.ToolStripButton bnMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton bnEditEmployee;
        private System.Windows.Forms.ToolStripButton bnSaveChanges;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton bnRefresh;
    }
}

