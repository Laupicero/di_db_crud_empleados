﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Globalization;

//  **************************************************
//      EJERCICIO DE REALIZACIÓN DE DB
//          PROFESOR: ALVARO LÓPEZ JURADO
//          ALUMNO: LAURA LUCENA BUENDIA
//  **************************************************
namespace Empleados
{
    public partial class frmEmpleados : Form
    {
        private SqlConnection conexion;
        private SqlDataAdapter sqlAdapter;
        private SqlCommandBuilder commandBuilder;

        private DataSet dataSet;
        private DataTable dataTable;
        private BindingSource bs;

        //Variable para una mejor insercción/actualización de los datos
        String metodoCRUD;
        

        //Constructor
        public frmEmpleados()
        {
            InitializeComponent();          
        }

        //LOAD
        private void frmEmpleados_Load(object sender, EventArgs e)
        {
            try
            {
                string cadenaDeConexion = @"Server=DESKTOP-GVEQ5O6;Database=BDPasaje; Integrated Security=true;";
                this.conexion = new SqlConnection(cadenaDeConexion);
                conexion.Open();
                MessageBox.Show("Conexion exitosa a SQL Server");
                this.metodoCRUD = "";

                //Adaptador
                this.sqlAdapter = new SqlDataAdapter("Select * from Empleado", this.conexion);
               
                //Rellenamos el DGV y lo 'bindeamos'
                rellenarEmpleadosDGV();

                //Para el CRUD
                this.commandBuilder = new SqlCommandBuilder(sqlAdapter);
                sqlAdapter.InsertCommand = this.commandBuilder.GetInsertCommand();
                sqlAdapter.InsertCommand = this.commandBuilder.GetDeleteCommand();
                sqlAdapter.InsertCommand = this.commandBuilder.GetUpdateCommand();

                rellenarItemsConDatosEmpleado(getEmpleadobyID(Convert.ToInt32(bnActualPositionItem.Text)));
                rellenarComboBox();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Problema al tratar de conectar a BD. Detalles: \n" + ex.Message.ToString());
            }
        }




        //---------------------------------------------------------------
        //          MÉTODOS-EVENTOS -BOTONES
        //---------------------------------------------------------------

        //----------------------------------
        // BOTONES DE ANTERIOR-SIGUIENTE : funcionan sólos gracias al Binding
        // Sólo nos rellena los datos de los campos para que se vea reflejado
        //----------------------------------
        private void bnMoveFirstItem_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dgvEmpleado[0, Convert.ToInt32(bnActualPositionItem.Text) - 1].Value.ToString());
            rellenarItemsConDatosEmpleado(getEmpleadobyID(id));
        }
        

        private void bnMovePreviousItem_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dgvEmpleado[0, Convert.ToInt32(bnActualPositionItem.Text) - 1].Value.ToString());
            rellenarItemsConDatosEmpleado(getEmpleadobyID(id));
        }

        private void bnMoveNextItem_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dgvEmpleado[0, Convert.ToInt32(bnActualPositionItem.Text) - 1].Value.ToString());
            rellenarItemsConDatosEmpleado(getEmpleadobyID(id));
        }

        private void bnMoveLastItem_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dgvEmpleado[0, Convert.ToInt32(bnActualPositionItem.Text) - 1].Value.ToString());
            rellenarItemsConDatosEmpleado(getEmpleadobyID(id));
        }



        //----------------------------------
        // BOTONES DE CRUD
        //----------------------------------

        //-------------
        // Eliminamos el Empleado que tengamos señalado
        private void bnDeleteEmployeeItem_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea borrar realmente a este Empleado?", "Borrar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Validate();
                dgvEmpleado.EndEdit();
                sqlAdapter.Update(this.dataTable);
                
            }
        }

        //-------------
        // Vacíamos los campos y los desactivamos para poder insertar un nuevo Empleado
        private void bnAddNewEmployee_Click(object sender, EventArgs e)
        {
            controlHabilitarDeshabilitarFormulario(true);
            limpiarCampos();
            dgvEmpleado.Enabled = false;
            this.metodoCRUD = "inserccionDatos";
        }
        //-------------


        //-------------
        // Modificamos los datos del Empleado que tengamos señalizado
        // Pero en el Panel
        private void bnEditEmployee_Click(object sender, EventArgs e)
        {
            controlHabilitarDeshabilitarFormulario(true);
            dgvEmpleado.Enabled = false;
            this.metodoCRUD = "actualizacionDatos";
        }
        //-------------


        //-------------
        // Guardamos los cambios a la hora de Insertar/Modificar los datos de un empleado
        private void bnSaveChanges_Click(object sender, EventArgs e)
        {
            if(this.metodoCRUD == "inserccionDatos")
            {
                if (todosCamposRellenos())
                {
                    insertarNuevoEmpleado();
                    rellenarEmpleadosDGV();
                }

            }else if(this.metodoCRUD == "actualizacionDatos")
            {
                if (todosCamposRellenos())
                {
                    modificarDatosEmpleado();
                    rellenarEmpleadosDGV();
                }

            }
            else
            {
                this.bs.EndEdit();
                this.sqlAdapter.Update(this.dataSet, "Empleado");
            }
            controlHabilitarDeshabilitarFormulario(false);
            dgvEmpleado.Enabled = true;
            this.metodoCRUD = "";
        }

        

        private void dgvEmpleado_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            this.bs.EndEdit();
            this.sqlAdapter.Update(this.dataSet, "Empleado");
        }
        //-------------

        //-------------
        // Actualizamos el listado de Empleados por si ha habido algún problema
        private void bnRefresh_Click(object sender, EventArgs e)
        {
            rellenarEmpleadosDGV();
        }
        //-------------





      

        //---------------------------------------------------------------
        //          MÉTODOS AUXILIARES - EXTRAS
        //---------------------------------------------------------------

        // Nos comprueba que todos los campos del formulario están rellenos
        // Nos devolverá un booleano
        // True si están todos rellenos
        // False si falta alguno de los campos
        private bool todosCamposRellenos()
        {
            if (txtNombre.TextLength > 0 && txtPaterno.TextLength > 0 && txtMaterno.TextLength > 0 && txtSueldo.TextLength > 0 &&
                dtpFechaContrato.Checked && cmbSexo.Text.Length >= 0 && cmbContrato.Text.Length >= 0 && cmbTipousuario.Text.Length >= 0)
                return true;
            else
            {
                MessageBox.Show("Debe rellenar primero todos los campos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }// Fin método todosCamposRellenos


        // Nos limpiará y reseteará todos los campos/componentes del formulario
        private void limpiarCampos()
        {
            lblCodigo.Text = "";
            txtNombre.Text = "";
            txtPaterno.Text = "";
            txtMaterno.Text = "";
            txtSueldo.Text = "";
            cmbSexo.Text = "";
            chkHabilitado.Checked = false;
            cmbContrato.Text = "";
            cmbTipousuario.Text = "";
        }// Fin método limpiarCampos


        //Nos rellena todos los componentes del formulario con los datos del empleado que le pasemos
        private void rellenarItemsConDatosEmpleado(Empleado empleado)
        {
            lblCodigo.Text = empleado.Id.ToString();
            txtNombre.Text = empleado.Nombre;
            txtPaterno.Text = empleado.Apll1;
            txtMaterno.Text = empleado.Apll2;
            txtSueldo.Text = empleado.Sueldo.ToString();
            dtpFechaContrato.Value = empleado.FechaContrato;
            cmbSexo.Text = consultaSexoEmpleado(empleado.IdSexo);
            chkHabilitado.Checked = Convert.ToBoolean(empleado.BHabilitado);
            cmbContrato.Text = consultaContratoEmpleado(empleado.IdTipoContrato);
            cmbTipousuario.Text = consultaEmpleadoTipoUsuario(empleado.TipoUsuario);
        }

        // Dependiendo de la acción que esté realizando el usuario
        // estarán habilitadas o no algunas funciones
        private void controlHabilitarDeshabilitarFormulario(bool estado)
        {
            txtNombre.Enabled = estado;
            txtPaterno.Enabled = estado;
            txtMaterno.Enabled = estado;
            txtSueldo.Enabled = estado;
            cmbSexo.Enabled = estado;
            dtpFechaContrato.Enabled = estado;
            chkHabilitado.Enabled = estado;
            cmbContrato.Enabled = estado;
            cmbTipousuario.Enabled = estado;
        }// Fin método controlHabilitarDeshabilitarFormulario






        //---------------------------------------------------------------
        //         MÉTODOS AUXILIARES - REALIZACIÓN DE CONSULTAS
        //---------------------------------------------------------------

        //------------------------
        ///// Nos insertará un nuevo empleado tanto en la DB como en el listado
        private void insertarNuevoEmpleado()
        {
            // Nos creamos el Empleado
            Empleado emp = new Empleado(
                txtNombre.Text,
                txtPaterno.Text,
                txtMaterno.Text,
                dtpFechaContrato.Value,
                Convert.ToDecimal(txtSueldo.Text),
                getIdTipoUsuario(cmbTipousuario.SelectedItem.ToString()),
                getIDContrato(cmbContrato.SelectedItem.ToString()),
                getIDTipoSexo(cmbSexo.SelectedItem.ToString()),
                Convert.ToInt32(chkHabilitado.Checked), 0, 'E'
                );

            // Insertamos en la DB
            insertarEmpleadoEnDB(emp);
        }
        //------------------------


        //------------------------
        //Nos realiza la 'query' para insertar en la DB al Empleado
        private void insertarEmpleadoEnDB(Empleado emp)
        {

            try
            {
                String query = "INSERT INTO Empleado(NOMBRE, APPATERNO, APMATERNO, FECHACONTRATO, SUELDO, IIDTIPOUSUARIO, " +
                "IIDTIPOCONTRATO, IIDSEXO, BHABILITADO, bTieneUsuario, TIPOUSUARIO)" +

                "VALUES('" + emp.Nombre + "', '" + emp.Apll1 + "', '" + emp.Apll2 + "', '" + emp.FechaContrato + "', " + emp.Sueldo + ", "
                + emp.IdTipoUsuario + ", " + emp.IdTipoContrato + ", " + emp.IdSexo + ", " + emp.BHabilitado + ", " + emp.TieneUsuario + ", '" + emp.TipoUsuario + "')";

                SqlCommand cmd = new SqlCommand(query, this.conexion);

                cmd.ExecuteNonQuery();
                MessageBox.Show("¡Nuevo Empleado Insertado con ÉXITO!", "OPERACIÓN REALIZADA");
                controlHabilitarDeshabilitarFormulario(false);
            }
            catch (Exception e)
            {
                MessageBox.Show("Vaya... Al parecer algo falló: \n" + e.Message, "ERROR");
            }
        }// Fin Método insertarEmpleadoEnDB
         //------------------------



        //------------------------
        /// Nos modificará/actualizará los datos de uno de nuestros empleados
        private void modificarDatosEmpleado()
        {
            // Nos creamos el Empleado
            Empleado emp = new Empleado(
                Convert.ToInt32(lblCodigo.Text),
                txtNombre.Text,
                txtPaterno.Text,
                txtMaterno.Text,
                dtpFechaContrato.Value,
                Convert.ToDecimal(txtSueldo.Text),
                getIdTipoUsuario(cmbTipousuario.Text),
                getIDContrato(cmbContrato.Text),
                getIDTipoSexo(cmbSexo.Text),
                Convert.ToInt32(chkHabilitado.Checked), 0, 'E'
                );

            // Actualizamos los datos en la DB
            modificarEmpleadoEnDB(emp);
        }// Fin Método modificarDatosEmpleado

        
        //Nos realiza la 'query' para modificar en la DB al Empleado
        private void modificarEmpleadoEnDB(Empleado emp)
        {
            try
            {
                /*String query = "UPDATE Empleado SET NOMBRE = '" + emp.Nombre + "', APPATERNO = '" + emp.Apll1 + "', APMATERNO = '" + emp.Apll2 + "', FECHACONTRATO = '" + Convert.ToDateTime(emp.FechaContrato.ToString("yyyy-MM-dd HH:mm:ss.fff")) + "'," +
                    "SUELDO = " + emp.Sueldo + ",  BHABILITADO = " + emp.BHabilitado + ", bTieneUsuario = " + emp.TieneUsuario + ", TIPOUSUARIO = '" + emp.TipoUsuario + "'" +
                    "Where IIDEMPLEADO = " + emp.Id;*/

                String query = "UPDATE Empleado SET NOMBRE = @nombre, APPATERNO = @apellido1, APMATERNO = @apellido2, FECHACONTRATO = @fecha, SUELDO = @sueldo" +
                    ",  BHABILITADO = @bhabilitado, bTieneUsuario = @btieneusuario, TIPOUSUARIO = @tipoUsuario Where IIDEMPLEADO = @id";

                SqlCommand command = new SqlCommand(query, this.conexion);

                command.Parameters.AddWithValue("@nombre", emp.Nombre);
                command.Parameters.AddWithValue("@apellido1", emp.Apll1);
                command.Parameters.AddWithValue("@apellido2", emp.Apll2);
                command.Parameters.AddWithValue("@fecha", Convert.ToDateTime(emp.FechaContrato.ToString("yyyy-MM-dd HH:mm:ss.fff")));

                // Para los números decimales
                SqlParameter parameter = new SqlParameter("@sueldo", SqlDbType.Decimal);
                // Precisión (lo que tienen entre paréntesis)
                parameter.Precision = 18;
                parameter.Scale = 2;
                // Le damos el valor que queremos
                parameter.Value = emp.Sueldo;

                // Add this parameter to your command
                command.Parameters.Add(parameter);

                command.Parameters.AddWithValue("@bhabilitado", emp.BHabilitado);
                command.Parameters.AddWithValue("@btieneusuario", emp.TieneUsuario);
                command.Parameters.AddWithValue("@tipoUsuario", emp.TipoUsuario);
                command.Parameters.AddWithValue("@id", emp.Id);

                //Ejecutamos la query
                command.ExecuteNonQuery();

                MessageBox.Show("¡Empleado Actualizado con ÉXITO!", "OPERACIÓN REALIZADA");
                controlHabilitarDeshabilitarFormulario(false);

            }
            catch (Exception e)
            {
                MessageBox.Show("Vaya... Al parecer algo falló: \n" + e.Message, "ERROR");
            }
        }
        //-------------


        // Nos devuelve un Empleado a partir de su 'ID'
        private Empleado getEmpleadobyID(int id)
        {
            Empleado emp = null;
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 * FROM Empleado WHERE IIDEMPLEADO >= " + id, this.conexion);
            SqlDataReader rd = cmd.ExecuteReader();

            //Rellenamos el listado de Empleados
            while (rd.Read())
            {
                try
                {
                    // PAra que no salté la excepción de nullReference
                    int bTieneUsuario = 1;
                    if (rd["bTieneUsuario"] == null)
                        bTieneUsuario = 0;

                    emp = new Empleado(Convert.ToInt32(rd[0]),
                        rd[1].ToString(), rd[2].ToString(), rd[3].ToString(),
                        Convert.ToDateTime(rd[4]),
                        Convert.ToDecimal(rd[5]),
                        Convert.ToInt32(rd[6]),
                        Convert.ToInt32(rd[7]),
                        Convert.ToInt32(rd[8]),
                        Convert.ToInt32(rd[9]),
                        bTieneUsuario,
                        Convert.ToChar(rd[11]));
                }
                catch (Exception e)
                {
                    MessageBox.Show("Excepción: " + e.Message, "ERROR");
                    rd.Close();
                }
            }
            rd.Close();
            return emp;
        }

        //-------------
        //Rellenamos los ComboBox a la hora en la que vayamos a insertar un nuevo Empleado
        private void rellenarComboBox()
        {
            SqlCommand cmd;
            SqlDataReader dataReader;

            cmbSexo.Items.Clear();
            cmbContrato.Items.Clear();
            cmbTipousuario.Items.Clear();

            //ComboBox-sexo
            cmd = new SqlCommand("Select nombre from Sexo", this.conexion);
            dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
                cmbSexo.Items.Add(dataReader["nombre"]);
            dataReader.Close();

            //ComboBox-TipoContrato
            cmd = new SqlCommand("Select nombre from tipocontrato", this.conexion);
            dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
                cmbContrato.Items.Add(dataReader["nombre"]);
            dataReader.Close();

            //ComboBox-TipoUsuario
            cmd = new SqlCommand("Select nombre from tipousuario", this.conexion);
            dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
                cmbTipousuario.Items.Add(dataReader["nombre"]);
            dataReader.Close();
        }
        //-------------


       


        //-------------
        /// Nos rellenará nuestro DataGridView de Empleados y lo bindeará
        // con el bindingNavigator
        private void rellenarEmpleadosDGV()
        {
            this.dataSet = new DataSet();
            this.sqlAdapter.Fill(this.dataSet, "Empleado");
            this.dataTable = this.dataSet.Tables["Empleado"];

            this.bs = new BindingSource();
            this.bs.DataSource = this.dataSet.Tables[0].DefaultView;

            bnEmpleado.BindingSource = this.bs;
            dgvEmpleado.DataSource = this.bs;


            /*
             * Rellenar DGV Y Bindear con BindingNav
             SqlDataAdapter sqlAdapter = new SqlDataAdapter("Select * from Empleado", this.conexion);
            DataSet ds = new DataSet();
            sqlAdapter.Fill(ds);

            BindingSource bs = new BindingSource();
            bs.DataSource = ds.Tables[0].DefaultView;
            bnEmpleado.BindingSource = bs;
            dgvEmpleado.DataSource = bs;
             
             */


            // Rellenar Sólo DGV
            /*String consulta = "Select * from Empleado";
            SqlDataAdapter sda = new SqlDataAdapter(consulta, this.conexion);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dgvEmpleado.DataSource = dt;*/
        }



        //---------------------------
        //  CONSULTAS MENORES
        //---------------------------

        //-------------
        // Nos devuelve a través de un 'char' el nombre del tipo de usuario
        private string consultaEmpleadoTipoUsuario(char tipoUsuario)
        {
            SqlCommand cmd;
            SqlDataReader dataReader;
            String tipoUsuarioString = "";
            cmd = new SqlCommand("SELECT NOMBRE FROM TIPOUSUARIOREGISTRO WHERE TIPOUSUARIO = '" + tipoUsuario + "'", this.conexion);

            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
                tipoUsuarioString = dataReader["NOMBRE"].ToString();

            dataReader.Close();

            return tipoUsuarioString;
        }// Fin Método consultaEmpleadoTipoUsuario
        //-------------


        //-------------
        // Nos devuelve a través de un 'id' el nombre del tipo de contrato
        private string consultaContratoEmpleado(int idTipoContrato)
        {
            SqlCommand cmd;
            SqlDataReader dataReader;
            String tipoContratoString = "";
            cmd = new SqlCommand("SELECT NOMBRE FROM TipoContrato WHERE IIDTIPOCONTRATO = " + idTipoContrato, this.conexion);

            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
                tipoContratoString = dataReader["NOMBRE"].ToString();

            dataReader.Close();
            return tipoContratoString;
        }// Fin Método consultaContratoEmpleado
        //-------------


        //-------------
        // Nos devuelve a través de un 'id' el tipo de sexo del empleado
        private string consultaSexoEmpleado(int idSexo)
        {
            SqlCommand cmd;
            SqlDataReader dataReader;
            String tipoSexo = "";
            cmd = new SqlCommand("SELECT NOMBRE FROM Sexo where IIDSEXO = " + idSexo, this.conexion);

            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
                tipoSexo = dataReader["NOMBRE"].ToString();

            dataReader.Close();
            return tipoSexo;
        }// Fin Método consultaSexoEmpleado
        //-------------


        //-------------
        // Nos devuelve el ID del sexo a través del nombre
        private int getIDTipoSexo(string nombreSexo)
        {
            SqlCommand cmd;
            SqlDataReader dataReader;
            int idTipoSexo = 0;
            cmd = new SqlCommand("select IIDSEXO from sexo where NOMBRE = '" +nombreSexo+"'", this.conexion);

            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
                idTipoSexo = Convert.ToInt32(dataReader["IIDSEXO"]);

            dataReader.Close();
            return idTipoSexo;
        }// Fin Método getIDTipoSexo
        //-------------


        //-------------
        // Nos devuelve el ID del tipo de contrato a través del nombre
        private int getIDContrato(string nombreContrato)
        {
            SqlCommand cmd;
            SqlDataReader dataReader;
            int id = 0;
            cmd = new SqlCommand("SELECT IIDTIPOCONTRATO FROM TipoContrato WHERE NOMBRE = '" +nombreContrato+ "'", this.conexion);

            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
                id = Convert.ToInt32(dataReader["IIDTIPOCONTRATO"]);

            dataReader.Close();
            return id;
        }// Fin Método getIDContrato
        //-------------


        //-------------
        // Nos devuelve el ID del tipo de Usuario a través del nombre
        private int getIdTipoUsuario(string nombreTipoUsuario)
        {
            SqlCommand cmd;
            SqlDataReader dataReader;
            int id = 0;
            cmd = new SqlCommand("SELECT IIDTIPOUSUARIO FROM TipoUsuario WHERE NOMBRE = '" +nombreTipoUsuario+ "'", this.conexion);

            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
                id = Convert.ToInt32(dataReader["IIDTIPOUSUARIO"]);

            dataReader.Close();
            return id;
        }// Fin Método getIDContrato getIdTipoUsuario

        private void dgvEmpleado_SelectionChanged(object sender, EventArgs e)
        {
            int idSeleccionado = Convert.ToInt32(dgvEmpleado.CurrentRow.Cells[0].Value.ToString());
            rellenarItemsConDatosEmpleado(getEmpleadobyID(idSeleccionado));
        }
    }
}
